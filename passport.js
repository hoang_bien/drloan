'use strict';
exports = module.exports = function (app, passport) {
    const JwtStrategy = require('passport-jwt').Strategy,
        LocalStrategy = require('passport-local').Strategy,
        ExtractJwt = require('passport-jwt').ExtractJwt;

    const opts = {};
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = app.config.jwt_secret;
    passport.use(new JwtStrategy(opts, function (jwt_payload, done) {
        app.db.models.User.findById(jwt_payload.id, function (err, user) {
            if (err) {
                return done(err, false);
            }
            if (user) {
                done(null, user);
            } else {
                done(null, false);
            }
        });
    }));

    passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    }, (email, password, cb) => {
        console.log('Authenticate');
        app.db.models.User.findOne({email: email}, function (err, user) {
            if (err) {
                return cb(err);
            }
            if (!user) {
                return cb(null, false);
            }
            app.db.models.User.validatePassword(password,user.password, (err, result) => {
                if (err) {
                    return cb(err);
                }
                if (result) {
                    return cb(null, user);
                }
                return cb(null, false);
            });
        });
    }));

};