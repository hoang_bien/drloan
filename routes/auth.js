const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const passport = require('passport');

router.post('/token', function (req, res, next) {
    if (!req.body.email || !req.body.password) {
        console.log("missing param");
        res.status(400).send();
    }
    passport.authenticate('local', {session: false}, (err, user, info) => {
        if (err || !user) {
            console.log(err);
            return res.status(400).json({
                message: 'Something is not right'
            });
        }
        req.login(user, {session: false}, (err) => {
            if (err) {
                res.send(err);
            }
            // generate a signed son web token with the contents of user object and return it in the response
            const token = jwt.sign({id: user._id}, req.app.config.jwt_secret);
            return res.json({"token": token});
        });


    })(req, res,next);
});

module.exports = router;