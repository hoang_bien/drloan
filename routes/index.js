var express = require('express');
var router = express.Router();
const passport = require('passport');

const moment = require('moment');

const Multer = require('multer');
const cloudinary = require('cloudinary');

const multer = Multer({
    storage: Multer.MemoryStorage,
    limits: {
        fileSize: 5 * 1024 * 1024 // no larger than 5mb
    }
});


const ensureDoctor = function (req, res, next) {
    if (req.isAuthenticated() && req.user.roles.doctor) {
        console.log('Authenticated role: ', req.user.roles.admin);
        return next();
    }
    //req.flash('error','Chưa đăng nhập');
    res.redirect("/login");
};

const ensureAdmin = function (req, res, next) {
    if (req.isAuthenticated() && req.user.roles.admin) {
        console.log('Authenticated role: ', req.user.roles.admin);
        return next();
    }
    req.flash('error', 'Chưa đăng nhập')
    res.redirect("/login");
};


router.get('/login', function (req, res, next) {
    res.render('login');
});

router.post('/login', function (req, res, next) {
    if (!req.body.email || !req.body.password) {
        console.log("missing param");
        res.status(400).send();
    }
    passport.authenticate('local', function (err, user, info) {
        if (err) {
            console.log(err);
            return next(err);
        }
        console.log(user);
        if (!user && !user.roles.doctor && !user.roles.admin) {
            req.flash('error', 'Sai tên tài khoản hoặc mật khẩu');
            return res.redirect('/login');
        }

        req.login(user, function (err) {
            if (err) {
                return next(err);
            }
            return res.redirect('/');
        });
    })(req, res, next);
});

router.post('/profile', ensureDoctor, function (req, res, next) {
    req.app.db.models.Doctor.findByIdAndUpdate(req.user.roles.doctor, {
        name: req.body.name,
        degree: req.body.degree
    }, function (err) {
        if (err) {
            console.log(err);
        }
        res.redirect('/doctor');
    });
});

router.get('/test.json', function (req, res, next) {
    return res.json({msg: "Hello"});
});

router.get('/chat.json', function (req, res, next) {
    req.app.db.models.Patient.find({doctor: req.query.id}).populate('last_message').then(msgs => res.json(msgs))
        .catch(e => {
            console.log(e);
            res.status(400).send(e.message);
        })
});

router.get('/mailbox', ensureDoctor, function (req, res, next) {
    const page = req.query.page || 1;
    const doctor = req.app.db.models.Doctor.findById(req.user.roles.doctor);
    const patients = req.app.db.models.Patient.paginate({
        doctor: req.user.roles.doctor
    }, {
        populate: "last_message",
        page: page,
        limit: 10
    });
    Promise.all([doctor, patients]).then(values => {
        values[0].email = req.user.email;
        values[0].roles = req.user.roles;
        res.render("mailbox", {
            doctor: values[0],
            patients: values[1]
        });
    }).catch(e => {
        console.log(e);
        res.status(400).send(e.message);
    })
});

router.get('/mailbox/:patientId', ensureDoctor, function (req, res, next) {
    const patient = req.app.db.models.Patient.findById(req.params.patientId).populate('user', 'email');
    const doctor = req.app.db.models.Doctor.findById(req.user.roles.doctor);
    const messages = req.app.db.models.Message.find({
        doctor: req.user.roles.doctor,
        patient: req.params.patientId
    }).sort('timestamp');
    Promise.all([patient, doctor, messages]).then(values => {
        const doctor = values[1];
        doctor.email = req.user.email;
        doctor.roles = req.user.roles;
        console.log(values[1])
        res.render('mail_single', {patient: values[0], doctor: doctor, messages: values[2]});
    }).catch(e => res.status(400).send(e.message));
});

router.post('/mailbox/:patientId', ensureDoctor, function (req, res, next) {
    req.app.db.models.Message.create({
        patient: req.params.patientId,
        doctor: req.user.roles.doctor,
        msg: req.body.msg,
        fromPatient: false
    }).then(message => {

        console.log('Sending notification');
        //Send notification
        req.app.db.models.Patient.findById(req.params.patientId)
            .populate('doctor').exec((err, patient) => {
            if (!err && patient && patient.push_token) {
                const payload = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
                    to: patient.push_token,
                    collapse_key: req.user.roles.doctor,
                    notification: {
                        title: patient.doctor.name,
                        body: message.msg
                    }
                };

                req.app.fcm.send(payload, function (err, response) {
                    if (err) {
                        console.log("Something has gone wrong!");
                    } else {
                        console.log("Successfully sent with response: ", response);
                    }
                });
            }
            else {
                console.log('Push token empty')
            }
        });
        res.redirect(req.params.patientId);
    });
});

router.get('/messages.json', function (req, res, next) {
    req.app.db.models.Message.find({doctor: req.query.doctor_id, patient: req.query.patient_id})
        .sort("timestamp")
        .then(msgs => {
            if (req.query.group && msgs.length > 0) {
                const groups = [];
                let lastDate = msgs[0].timestamp;
                let lastFromPatient = msgs[0].fromPatient;
                let dayBuffer = [];
                let minBuffer = [msgs[0]];
                for (let i = 1; i < msgs.length; i++) {
                    const curDate = msgs[i].timestamp;
                    if (lastDate.getDay() === curDate.getDay() &&
                        lastDate.getMonth() === curDate.getMonth() &&
                        lastDate.getFullYear() === curDate.getFullYear()) {
                        if (curDate.getTime() - lastDate.getTime() <= 60000 && msgs[i].fromPatient === lastFromPatient) {
                            minBuffer.push(msgs[i]);
                        } else {
                            dayBuffer.push(minBuffer);
                            minBuffer = [msgs[i]];
                        }
                    } else {
                        groups.push(dayBuffer);
                        dayBuffer = [];
                        minBuffer = [msgs[i]];
                    }
                    lastDate = curDate;
                }
                //Flush the buffer
                dayBuffer.push(minBuffer);
                groups.push(dayBuffer);
                res.json(groups);
            } else {
                res.json(msgs);
            }
        })
        .catch(e => {
            console.log(e);
            res.status(400).send(e.message);
        })

});

router.get('/', ensureDoctor, function (req, res, next) {
    const page = req.query.page || 1;
    const limit = req.query.limit || 10;
    const doctor = req.app.db.models.Doctor.findById(req.user.roles.doctor).then(doctor => {
        doctor.email = req.user.email;
        doctor.roles = req.user.roles;
        return doctor;
    });

    const patients = req.app.db.models.Patient.paginate({
        doctor: req.user.roles.doctor
    }, {
        populate: [{
            path: 'user',
            select: 'email'
        }, 'latest_visit'],
        page: page,
        limit: limit
    });

    Promise.all([doctor, patients]).then(values => {
        res.render('index', {doctor: values[0], patients: values[1]})
    }).catch(e => res.send(e.message));
    // patients.then(result => {
    //     console.log(result);
    //     res.send(result);
    // }).catch(err => {
    //     console.log(err);
    //     res.status(400).send(err.message);
    // })

});

router.get('/records', ensureDoctor, function (req, res, next) {
    const patient_id = req.query.patient_id;
    const patient = req.app.db.models.Patient.findById(patient_id);
    const records = req.app.db.models.MedicalHistory.find({patient: patient_id});
    const doctor = req.app.db.models.Doctor.findById(req.user.roles.doctor);
    Promise.all([patient, doctor, records]).then(values => {
        res.render('med_record.ejs', {patient: values[0], doctor: values[1], records: values[2]})
    }).catch(e => res.send(e.message));
});

router.get('/patient/:patientId/all', ensureDoctor, async (req, res) => {
    try {
        const patient_id = req.params.patientId;

        const patient = await req.app.db.models.Patient.findById(patient_id).populate('user', 'email');
        const records = await req.app.db.models.MedicalHistory.find({patient: patient_id});
        const doctor = await req.app.db.models.Doctor.findById(req.user.roles.doctor);
        doctor.email = req.user.email;
        doctor.roles = req.user.roles;


        const sitting = await req.app.db.models.SittingTime.aggregate([
            {$match: {chairId: patient.chairId}},
            {
                $group: {
                    _id: {month: {$month: "$timeStart"}, day: {$dayOfMonth: "$timeStart"}, year: {$year: "$timeStart"}},
                    totalTime: {
                        $sum: {$subtract: ["$timeEnd", "$timeStart"]}
                    },
                    sortKey: {"$first": "$timeStart"},
                    count: {$sum: 1}
                }
            },
            {$sort: {sortKey: 1}},
            {$limit: 10}
        ]);
        console.log(sitting);
        const sittingStats = {
            labels: sitting.map(x => moment([x._id.year, x._id.month - 1, x._id.day]).format('dddd, DD-MM-YYYY')),
            timePerDays: sitting.map(x => Math.round(x.totalTime / 360000) / 10),
            countPerDays: sitting.map(x => x.count)
        };

        res.render('patient_detail.ejs', {
            patient: patient,
            doctor: doctor,
            records: records,
            sittingStats: sittingStats
        });
    } catch (e) {
        console.log('Mongo Error: ', e);
        res.status(500).send();
    }
});

router.get('/patient/:patientId/test_results', ensureDoctor, async (req, res) => {
    try {
        const patient_id = req.params.patientId;

        const patient = await req.app.db.models.Patient.findById(patient_id).populate('user', 'email');
        const records = await req.app.db.models.MedicalHistory.find({patient: patient_id}).sort({date: -1});
        const doctor = await req.app.db.models.Doctor.findById(req.user.roles.doctor);
        doctor.email = req.user.email;
        doctor.roles = req.user.roles;

        res.render('patient_test_result.ejs', {
            patient: patient,
            doctor: doctor,
            records: records
        });
    } catch (e) {
        console.log('Mongo Error: ', e);
        res.status(500).send();
    }
});

router.post('/patient/:patientId/test_results', ensureDoctor, async (req, res) => {
    const data = {
        name: req.body.name,
        doctorName: req.body.doctorName,
        date: moment(req.body.date + " " + req.body.time, "MM-DD-YYYY HH:mm").toDate(),
        symptoms: req.body.symptoms,
        conclusion: req.body.conclusion,
        treatment: req.body.treatment,
        note: "",
        patient: req.params.patientId
    };
    try {
        await req.app.db.models.MedicalHistory.create(data);
        res.redirect('/patient/' + req.params.patientId + '/test_results');
    } catch (e) {
        console.log('Mongo Error: ', e);
        res.status(500).send();
    }
});
router.get('/patient/:patientId/test_results/new', function (req, res, next) {
    console.log('hi');
    res.render('med_record_add_or_edit');
});

router.get('/patient/:patientId/sitting', ensureDoctor, async (req, res) => {
    try {
        const patient_id = req.params.patientId;

        const patient = await req.app.db.models.Patient.findById(patient_id).populate('user', 'email');
        const sittingTimes = await req.app.db.models.SittingTime.find({chairId: patient.chairId}).sort({timeStart: -1});
        const doctor = await req.app.db.models.Doctor.findById(req.user.roles.doctor);
        doctor.email = req.user.email;
        doctor.roles = req.user.roles;
        res.render('patient_sitting.ejs', {
            patient: patient,
            doctor: doctor,
            sittingTimes: sittingTimes
        });
    } catch (e) {
        console.log('Mongo Error: ', e);
        res.status(500).send();
    }
});

router.get('/logout', function (req, res, next) {
    console.log('logging out');
    req.logout();
    res.redirect('/login');
});

// Admin permission

router.get('/account', ensureAdmin, async (req, res) => {
    try {
        const doctor = await req.app.db.models.Doctor.findById(req.user.roles.doctor);
        doctor.email = req.user.email;
        doctor.roles = req.user.roles;
        const totalPatients = await req.app.db.models.Patient.count();
        const totalDoctors = await req.app.db.models.Doctor.count();
        res.render('account_mgr', {doctor: doctor, totalPatients: totalPatients, totalDoctors: totalDoctors});
    } catch (e) {
        res.status(500).send('Mongo Error: ' + e.message);
    }
});

router.get('/account/doctors.json', ensureAdmin, async (req, res) => {
    try {
        const doctors = await req.app.db.models.Doctor.find().populate('user');
        console.log(doctors[0].user.roles);
        const out = doctors.filter(doc => doc.user.roles.admin);
        console.log(out);

        res.json(out);
    } catch (e) {
        res.status(500).send('Mongo Error: ' + e.message);
    }
});

router.delete('/account', ensureAdmin, async (req, res) => {
    console.log('Delete');
    try {
        const user = await req.app.db.models.User.findOne({email: req.body.email});
        if (user.roles.doctor) {
            console.log("Removing ", user.email);
            await req.app.db.models.Doctor.deleteOne({_id: user.roles.doctor});
        } else if (user.roles.patient) {
            console.log("Removing ", user.roles.patient);
            await req.app.db.models.Patient.deleteOne({_id: user.roles.patient});
        }
        await user.remove();
        res.status(200).send();
    } catch (e) {
        console.log(e);
        res.status(500).send('Mongo Error: ' + e.message);
    }
});

router.get('/account/patients.json', ensureAdmin, async (req, res) => {
    try {
        const patients = await req.app.db.models.Patient.find().populate('user');

        res.json(patients);
    } catch (e) {
        res.status(500).send('Mongo Error: ' + e.message);
    }
});


router.get('/doctors', ensureAdmin, function (req, res, next) {
    req.app.db.models.Doctor.find().populate('user').exec(function (err, doctors) {
        if (err) {
            res.status(500).send();
        }
        res.json(doctors);
    })
});

router.get('/patients', ensureAdmin, function (req, res, next) {
    req.app.db.models.Patient.find().populate('user').exec(function (err, patients) {
        if (err) {
            res.status(500).send();
        }
        res.json(patients);
    })
});

router.post('/admins', ensureAdmin, function (req, res, next) {
    if (!req.body.email || !req.body.password) {
        return res.status(400).send();
    }
    const first = req.body.first || "Default";
    const fullname = [req.body.last, req.body.middle, first].filter(w => w).join(' ');
    req.app.db.models.Admin.create({
            name: {first: first, middle: req.body.middle, last: req.body.last, full: fullname},
            groups: ['root']
        }
        , (err, admin) => {
            if (err) {
                res.status(400).send();
            }
            req.app.db.models.User.encryptPassword(req.body.password, (err, hash) => {
                if (err) {
                    return res.status(500).send();
                }
                req.app.db.models.User.create({
                    email: req.body.email, password: hash, roles: {
                        admin: admin._id
                    }
                }, function (err, newUser) {
                    if (err) {
                        console.log(err)
                        res.status(500).send();
                    }

                    admin.user = newUser._id;
                    admin.save((err, updatedAdmin) => {
                        if (err) {
                            res.status(500).send();
                        }
                        updatedAdmin.user = newUser;
                        res.json(updatedAdmin);
                    });
                })


            });
        });


    //
    //
    // });
});


router.post('/account/patients', ensureAdmin, function (req, res, next) {
    if (!req.body.email || !req.body.password || !req.body.chairId) {
        return res.status(400).send({msg: "Email or password or chair id is missing"});
    }
    const fullname = req.body.name || "No Name";


    req.app.db.models.Patient.create({
            name: fullname,
            currentChairId: req.body.chairId
        }
        , (err, patient) => {
            if (err) {
                console.log(err);
                res.status(400).send({msg: err.message});
            }
            req.app.db.models.User.encryptPassword(req.body.password, (err, hash) => {
                if (err) {
                    return res.status(500).send();
                }
                req.app.db.models.User.create({
                    email: req.body.email, password: hash, roles: {
                        patient: patient._id
                    }
                }, function (err, newUser) {
                    if (err) {
                        console.log(err);
                        res.status(500).send();
                    }

                    patient.user = newUser._id;
                    patient.save((err, updatedPatient) => {
                        if (err) {
                            console.log(err);
                            res.status(500).send();
                        }
                        updatedPatient.user = newUser;
                        res.redirect('/account')
                    });
                })


            });
        });


    //
    //
    // });
});

router.post('/account/doctor', ensureAdmin, function (req, res, next) {
    if (!req.body.email || !req.body.password) {
        return res.status(400).send();
    }
    const name = req.body.name || "No Name";
    req.app.db.models.Doctor.create({
        name: name,
        degree: req.body.degree
    }, (err, doctor) => {
        if (err) {
            res.status(400).send();
        }
        req.app.db.models.User.encryptPassword(req.body.password, (err, hash) => {
            if (err) {
                return res.status(500).send();
            }
            req.app.db.models.User.create({
                email: req.body.email, password: hash, roles: {
                    doctor: doctor._id
                }
            }, function (err, newUser) {
                if (err) {
                    console.log(err);
                    res.status(500).send();
                }

                doctor.user = newUser._id;
                doctor.save((err, updatedDoctor) => {
                    if (err) {
                        res.status(500).send();
                    }
                    //updatedDoctor.user = newUser;
                    //res.json(updatedDoctor);
                    res.redirect('/account')
                });
            })


        });
    });


    //
    //
    // });
});

router.post('/account/patient', ensureAdmin, function (req, res, next) {
    if (!req.body.email || !req.body.password) {
        return res.status(400).send();
    }
    const name = req.body.name || "No Name";
    req.app.db.models.Patient.create({
        name: name,
        chairId: req.body.chairId
    }, (err, patient) => {
        if (err) {
            res.status(400).send();
        }
        req.app.db.models.User.encryptPassword(req.body.password, (err, hash) => {
            if (err) {
                return res.status(500).send();
            }
            req.app.db.models.User.create({
                email: req.body.email, password: hash, roles: {
                    patient: patient._id
                }
            }, function (err, newUser) {
                if (err) {
                    console.log(err);
                    res.status(500).send();
                }

                doctor.user = newUser._id;
                doctor.save((err, updatedDoctor) => {
                    if (err) {
                        res.status(500).send();
                    }
                    //updatedDoctor.user = newUser;
                    //res.json(updatedDoctor);
                    res.redirect('/account')
                });
            })


        });
    });


    //
    //
    // });
});

router.get('/news', ensureAdmin, async (req, res, next) => {
    try {

        const doctor = await req.app.db.models.Doctor.findById(req.user.roles.doctor);
        doctor.email = req.user.email;
        doctor.roles = req.user.roles;
        req.app.db.models.Article.find().then(articles => res.render('news_list', {
            doctor: doctor,
            articles: articles
        }));
    } catch (e) {
        res.status(500).send('Mongo error: ' + e.message);
    }
});

router.get('/news/editor', ensureAdmin, function (req, res, next) {
    res.render('news_editor');
});

router.get('/news/remove/:articleId', ensureAdmin, async (req, res, next) => {
    try {
        await req.app.db.models.Article.deleteOne({_id:req.params.articleId});
        res.redirect('/news');
    } catch (e) {
        res.status(500).send('Mongo error: ' + e.message);
    }
});

router.get('/news/:articleId', ensureAdmin, function (req, res, next) {
    req.app.db.models.Article.findById(req.params.articleId).then(article => {
        res.render('news_single', {article: article});
    }).catch(e => res.status(404).send(e.message));
});

router.post('/news/publish', ensureAdmin, multer.single('image'), function (req, res, next) {
    console.log(req.file);
    if (req.file) {
        cloudinary.v2.uploader.upload_stream({resource_type: 'image'},
            function (error, result) {
                if (!error) {
                    save_article(req, res, result.url);
                } else {
                    save_article(req, res, null);
                }
            })
            .end(req.file.buffer);
    } else {
        save_article(req, res);
    }
});

function save_article(req, res, imgUrl) {

    req.app.db.models.Article.create({
        title: req.body.title,
        content: req.body.content,
        imgUrl: imgUrl
    }, function (err) {
        if (!err) {
            res.redirect('/news');
        } else {
            res.status(500).send(err.message);
        }
    })
}


module.exports = router;