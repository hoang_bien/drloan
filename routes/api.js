const express = require('express');
const router = express.Router();
const moment = require('moment');
var url = require('url');
var multer = require('multer');
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/images/uploads')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + getExtension(file))
    }
});


function getExtension(file) {
    // this function gets the filename extension by determining mimetype. To be exanded to support others, for example .jpeg or .tiff
    var res = '';
    if (file.mimetype === 'image/jpeg') res = '.jpg';
    if (file.mimetype === 'image/png') res = '.png';
    return res;
}

function fileFilter(req, file, cb) {
    var type = file.mimetype;
    var typeArray = type.split("/");
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
}

var upload = multer({storage: storage, limits: {fileSize: 2048576}, fileFilter: fileFilter});
const jwt = require('jsonwebtoken');


//For all user


avatarUpload = upload.single('avatar');

router.post('/avatar', function (req, res, next) {
    avatarUpload(req, res, err => {
        if (!err && req.file) {
            res.json({
                path: url.format({
                    protocol: req.protocol,
                    host: req.get('host'),
                    pathname: '/images/uploads/' + req.file.filename
                })
            });
        } else {
            res.status(406).send();
        }
    })
});

router.get('/profile', function (req, res, next) {
    const user = req.user;
    if (user.roles.patient) {
        req.app.db.models.Patient.findById(user.roles.patient, (err, patient) => {
            if (err) {
                res.status(400).send(err.message);
            }
            res.json(getPatientProfile(user, patient));
        });
    } else if (user.roles.doctor) {
        req.app.db.models.Doctor.findById(user.roles.doctor, (err, doctor) => {
            if (err) {
                res.status(400).send(err.message);
            }
            res.json(getDoctorProfile(user, doctor));
        });
    }
});

router.put('/profile', function (req, res, next) {
    const user = req.user;
    if (user.roles.patient) {
        req.app.db.models.Patient.findById(user.roles.patient, (err, patient) => {
            if (err) {
                res.status(400).send(err.message);
            }

            patient.phone = req.body.phone || patient.phone;
            patient.isMale = req.body.isMale === undefined || patient.isMale;
            patient.name = req.body.name || patient.name;
            if (req.body.address) {
                patient.address = {
                    street: req.body.address.street,
                    district: req.body.address.district,
                    city: req.body.address.city
                }
            }

            patient.imgUrl = req.body.imgUrl || patient.imgUrl;
            if (req.body.birthday) {
                patient.birthday = new Date(req.body.birthday);
            }

            patient.save((err, updatedPatient) => {
                if (err) {
                    res.status(400).send(err.message);
                }
                res.status(200).send();
                //res.json(getPatientProfile(user, updatedPatient));
            });


        });
    } else {
        res.status(400);
    }
});
/* Patient only*/

router.post('/push_token', function (req, res, next) {
    if (req.user.roles.patient && req.body.push_token) {
        req.app.db.models.Patient.findByIdAndUpdate(req.user.roles.patient, {push_token: req.body.push_token}, err => {
            if (err) {
                console.log(err.message);
                res.status(400).send({msg: err.message});
            } else {
                res.status(200).send({msg: "success"});
            }
        })
    } else {
        res.status(403).send({msg: "user must be a patient"});
    }
});

router.get('/my_doctor', function (req, res, next) {
    if (req.user.roles.patient) {

        req.app.db.models.Patient.findById(req.user.roles.patient).populate('doctor').exec((err, patient) => {
            if (err) {
                res.status(400).send(err.message);
            }
            res.json(patient.doctor);
        })
    } else {
        res.status(400).send({'msg': 'The user is not a patient'})
    }
});


router.get('/my_exams', function (req, res,) {
    if (req.user.roles.patient) {
        let query = req.app.db.models.MedicalHistory.find({patient: req.user.roles.patient});
        console.log(typeof req.query.limit);
        const limit = parseInt(req.query.limit);
        if (!isNaN(limit)) {
            query = query.limit(limit);
        }
        query.sort({date: -1}).exec((err, results) => {
            if (err) {
                res.status(404).send(err.message);
            }
            res.json(results);
        });
    } else {
        res.status(400).send({'msg': 'The user is not a patient'})
    }
});

/* Doctor only*/
router.get('/my_patients', function (req, res, next) {
    if (req.user.roles.doctor) {
        req.app.db.models.Patient.find({'doctor': req.user.roles.doctor})
            .populate('user')
            .then(patients => {
                res.json(patients.map(p => getPatientProfile(p.user, p)));
            })
            .catch(e => {
                res.status(400).send(e.message);
            })
    } else {
        res.status(400).send({'msg': 'The user is not a doctor'})
    }
});

router.get('/medical_records/', function (req, res, next) {

    console.log(req.query.patient_id);
    if (!req.query.id) {
        res.status(400).send('Missing user id param');
        return;
    }
    if (req.user.roles.doctor) {
        req.app.db.models.MedicalHistory
            .find({patient: req.query.id})
            .sort({date: -1})
            .then(results => res.json(results))
            .catch(e => res.status(404).send(e.message));
    } else {
        res.status(400).send({'msg': 'The user is not a doctor'})
    }
});

router.post('/medical_records', function (req, res, next) {
    console.log(req.query.patient_id);
    if (!req.query.id) {
        res.status(400).send('Missing user id param');
        return;
    }

    if (req.user.roles.doctor) {
        const date = new Date(req.body.date);
        req.app.db.models.MedicalHistory.create({
            name: req.body.name,
            doctorName: req.body.doctorName,
            symptoms: req.body.symptoms,
            conclusion: req.body.conclusion,
            treatment: req.body.treatment,
            note: req.body.no,
            patient: req.query.id,
            date: date
        }).then(record => {
            //Implement here
            req.app.db.models.Patient.findByIdAndUpdate(req.body.id, {latest_visit: record._id});
            res.json(record);
        })
            .catch(e => res.status(400).send(e.message));
    } else {
        res.status(400).send({'msg': 'The user is not a doctor'})
    }

});

router.put('/medical_records', function (req, res, next) {
    if (!req.query.id) {
        res.status(400).send('Missing user id param');
        return;
    }
    if (req.user.roles.doctor) {
        req.app.db.models.MedicalHistory.findById(user.roles.patient, (err, record) => {
            if (err || !record) {
                res.status(400).send(err.message);
            }

            record.name = req.body.name || record.name;
            record.doctorName = req.body.doctorName || record.doctorName;
            record.symptoms = req.body.symptoms || record.symptoms;
            record.conclusion = req.body.conclusion || record.conclusion;
            record.treatment = req.body.treatment || record.treatment;
            record.note = req.body.note || record.note;
            record.patient = req.query.id;
            if (req.query.date) {
                record.date = new Date(req.query.date);
            }

            record.save((err, updatedRecord) => {
                if (err) {
                    res.status(400).send(err.message);
                }
                res.status(200).send();
            });


        });
    } else {
        res.status(400).send('User is not a doctor');
    }
});

router.delete('/medical_records', function (req, res, next) {
    if (!req.query.id) {
        res.status(400).send('Missing user id param');
        return;
    }
    if (req.user.roles.doctor) {
        req.app.db.models.MedicalHistory.deleteOne({'_id': req.query.id}, function (err) {
            if (err) {
                res.status(400).send(err.message);
            } else {
                res.status(200).send();
            }
        })
    }
    else {
        res.status(400).send('User is not a doctor');
    }

});

router.get('/chat', function (req, res, next) {
    let patient_id = null;
    if (req.user.roles.patient) {
        patient_id = req.user.roles.patient
    } else if (req.user.roles.doctor && req.query.patient_id) {
        patient_id = req.query.patient_id;
    }
    console.log(patient_id);
    if (patient_id) {
        req.app.db.models.Message.find({patient: patient_id}).then(msgs => {
            res.json(msgs);
        }).catch(e => res.status(400).send(e.message));
    } else {
        res.status(403).send();
    }
});


router.post('/chat', function (req, res, next) {
    if (req.body.msg && req.body.msg.length > 0) {
        if (req.user.roles.patient) {
            req.app.db.models.Patient.findById(req.user.roles.patient)
                .then(patient => {
                    return req.app.db.models.Message.create({
                        patient: patient._id,
                        doctor: patient.doctor,
                        msg: req.body.msg,
                        fromPatient: true
                    });
                })
                .then(msg => {
                    res.json(msg);
                })
                .catch(e => {
                    res.status(400).send(e.message);
                });
        } else if (req.user.roles.doctor && req.query.patient_id) {
            req.app.db.models.Message.create({
                patient: req.query.patient_id,
                doctor: req.user.roles.doctor,
                msg: req.body.msg,
                fromPatient: false
            }).then(message => {

                console.log('Sending notification');
                //Send notification
                req.app.db.models.Patient.findById(req.query.patient_id)
                    .populate('doctor').exec((err, patient) => {
                    if (!err && patient.push_token) {
                        const payload = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
                            to: patient.push_token,
                            collapse_key: req.user.roles.doctor,
                            notification: {
                                title: patient.doctor.name,
                                body: message.msg
                            }
                        };

                        req.app.fcm.send(payload, function (err, response) {
                            if (err) {
                                console.log("Something has gone wrong!");
                            } else {
                                console.log("Successfully sent with response: ", response);
                            }
                        });
                    }
                    else {
                        console.log('Push token empty')
                    }
                });
                res.json(message);
            }).catch(e => {
                console.log(e);
                res.status(400).send(e.message);
            });

        } else {
            res.status(400).send();
        }
    } else {
        res.status(400).send();
    }

});

router.get('/news', function (req, res, next) {
    if (req.query.page || req.query.limit) {
        const page = req.query.page || 1;
        const limit = req.query.limit || 10;
        res.json('Not implemented');
    } else {
        req.app.db.models.Article.find().sort({timestamp: -1}).then(items => res.json(items))
            .catch(e => res.status(500).send(e.message));
    }
});
// sitting
router.get('/sittings/detail', async (req, res) => {
    if (req.query.date && req.user.roles.patient) {
        try {
            const lowerDate = new Date(req.query.date);
            //lowerDate.setHours(0,0,0);
            const upperDate = moment(lowerDate).add(1, 'd').toDate();
            console.log(req.query.date, lowerDate, upperDate);

            const user = await req.app.db.models.Patient.findById(req.user.roles.patient, 'chairId');
            console.log(user.chairId);
            const records = await req.app.db.models.SittingTime.find({chairId: user.chairId})
                .where('timeStart').gte(lowerDate).lt(upperDate)
                .sort({timeStart: -1});
            //console.log(records);
            // for(const rec in records){
            //     console.log(rec);
            //     console.log(rec.timeStart.getTime() - new Date(2018,10,30));
            // }
            res.json(records);
        } catch (e) {
            console.log('Mongo Error: ', e);
            return res.status(500).send();
        }
    }
    else {
        res.status(400).send();
    }
});

router.get('/sittings/all', async (req, res) => {
    if (!req.user.roles.patient) {
        return res.status(403).send();
    }
    try {
        const patient = await req.app.db.models.Patient.findById(req.user.roles.patient, 'chairId');

        const records = await req.app.db.models.SittingTime.aggregate([
                {$match: {chairId: patient.chairId}},
                {
                    $group: {
                        _id: {month: {$month: "$timeStart"}, day: {$dayOfMonth: "$timeStart"}, year: {$year: "$timeStart"}},
                        totalTime: {
                            $sum: {$subtract: ["$timeEnd", "$timeStart"]}
                        },
                        count: {$sum: 1}
                    }
                }
            ])
        ;
        res.json(records);
    } catch (e) {
        console.log('Mongo error: ', e);
        return res.status(500).send();
    }
});


function getPatientProfile(user, patient) {
    let birthday = undefined;
    if (patient.birthday) {
        birthday = patient.birthday.toJSON();
    }
    const returnedUser = {
        id: patient._id,
        email: user.email,
        isInitProfile: patient.isInitProfile,
        name: patient.name,
        isMale: patient.isMale,
        phone: patient.phone,
        address: patient.address,
        imgUrl: patient.imgUrl,
        birthday: birthday
    };
    return returnedUser
}

function getDoctorProfile(user, doctor) {
    return {
        id: doctor._id,
        email: user.email,
        name: doctor.name,
        degree: doctor.degree,
        imgUrl: doctor.imgUrl,
    }
}

module.exports = router;
