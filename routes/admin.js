var express = require('express');
var router = express.Router();
const passport = require('passport');
const Multer = require('multer');
const cloudinary = require('cloudinary');

const multer = Multer({
    storage: Multer.MemoryStorage,
    limits: {
        fileSize: 5 * 1024 * 1024 // no larger than 5mb
    }
});

function getExtension(file) {
    // this function gets the filename extension by determining mimetype. To be exanded to support others, for example .jpeg or .tiff
    var res = '';
    if (file.mimetype === 'image/jpeg') res = '.jpg';
    if (file.mimetype === 'image/png') res = '.png';
    return res;
}

// const multer = Multer({
//     storage: Multer.diskStorage({
//         destination: (req, file, cb) => {
//             cb(null, 'public/images/uploads')
//         },
//         filename: (req, file, cb) => {
//             cb(null, file.fieldname + '-' + Date.now() + getExtension(file))
//         }
//     }),
//     limits: {
//         fileSize: 5 * 1024 * 1024 // no larger than 5mb
//     }
// });


const ensureAdmin = function (req, res, next) {
    if (req.isAuthenticated() && req.user.roles.admin) {
        console.log('Authenticated role: ', req.user.roles.admin);
        return next();
    }
    req.flash('error', 'Chưa đăng nhập')
    res.redirect("/admin/login");
};

router.get('/', ensureAdmin, function (req, res, next) {
    res.redirect('/admin/account');
});
/* GET users listing. */
router.get('/account', ensureAdmin, async (req, res) => {
    try {
        const totalPatients = await req.app.db.models.Patient.count();
        const totalDoctors = await req.app.db.models.Doctor.count();
        res.render('admin/account_mgr', {totalPatients: totalPatients, totalDoctors: totalDoctors});
    }catch (e) {
        res.status(500).send('Mongo Error: ' + e.message);
    }
});

router.get('/login', function (req, res, next) {
    res.render('login', {loginUrl: "/admin/login"});
});

router.post('/login', function (req, res, next) {
    if (!req.body.email || !req.body.password) {
        console.log("missing param");
        res.status(400).send();
    }
    passport.authenticate('local', function (err, user, info) {
        if (err) {
            console.log(err);
            return next(err);
        }
        if (!user || !user.roles.admin) {
            req.flash('error', 'Sai tên tài khoản hoặc mật khẩu');
            return res.redirect('/admin/login');
        }
        req.login(user, function (err) {
            if (err) {
                return next(err);
            }
            return res.redirect('/admin/account');
        });
    })(req, res, next);
});

router.get('/admins', ensureAdmin, function (req, res, next) {
    req.app.db.models.Admin.find().populate('user').exec(function (err, admins) {
        if (err) {
            res.status(500).send();
        }
        res.json(admins);
    })
});

router.get('/account/doctors.json', async (req, res) => {
    try {
        const doctors = await req.app.db.models.Doctor.find().populate('user');
        res.json(doctors);
    } catch (e) {
        res.status(500).send('Mongo Error: ' + e.message);
    }
});


router.get('/account/patients.json', async (req, res) => {
    try {
        const patients = await req.app.db.models.Patient.find().populate('user');
        res.json(patients);
    } catch (e) {
        res.status(500).send('Mongo Error: ' + e.message);
    }
});


router.get('/doctors', ensureAdmin, function (req, res, next) {
    req.app.db.models.Doctor.find().populate('user').exec(function (err, doctors) {
        if (err) {
            res.status(500).send();
        }
        res.json(doctors);
    })
});

router.get('/patients', function (req, res, next) {
    req.app.db.models.Patient.find().populate('user').exec(function (err, patients) {
        if (err) {
            res.status(500).send();
        }
        res.json(patients);
    })
});

router.post('/admins', ensureAdmin, function (req, res, next) {
    if (!req.body.email || !req.body.password) {
        return res.status(400).send();
    }
    const first = req.body.first || "Default";
    const fullname = [req.body.last, req.body.middle, first].filter(w => w).join(' ');
    req.app.db.models.Admin.create({
            name: {first: first, middle: req.body.middle, last: req.body.last, full: fullname},
            groups: ['root']
        }
        , (err, admin) => {
            if (err) {
                res.status(400).send();
            }
            req.app.db.models.User.encryptPassword(req.body.password, (err, hash) => {
                if (err) {
                    return res.status(500).send();
                }
                req.app.db.models.User.create({
                    email: req.body.email, password: hash, roles: {
                        admin: admin._id
                    }
                }, function (err, newUser) {
                    if (err) {
                        console.log(err)
                        res.status(500).send();
                    }

                    admin.user = newUser._id;
                    admin.save((err, updatedAdmin) => {
                        if (err) {
                            res.status(500).send();
                        }
                        updatedAdmin.user = newUser;
                        res.json(updatedAdmin);
                    });
                })


            });
        });


    //
    //
    // });
});


router.post('/account/patients', function (req, res, next) {
    if (!req.body.email || !req.body.password || !req.body.chairId) {
        return res.status(400).send({msg: "Email or password or chair id is missing"});
    }
    const fullname = req.body.name || "No Name";


    req.app.db.models.Patient.create({
            name: fullname,
            currentChairId: req.body.chairId
        }
        , (err, patient) => {
            if (err) {
                console.log(err);
                res.status(400).send({msg: err.message});
            }
            req.app.db.models.User.encryptPassword(req.body.password, (err, hash) => {
                if (err) {
                    return res.status(500).send();
                }
                req.app.db.models.User.create({
                    email: req.body.email, password: hash, roles: {
                        patient: patient._id
                    }
                }, function (err, newUser) {
                    if (err) {
                        console.log(err);
                        res.status(500).send();
                    }

                    patient.user = newUser._id;
                    patient.save((err, updatedPatient) => {
                        if (err) {
                            console.log(err);
                            res.status(500).send();
                        }
                        updatedPatient.user = newUser;
                        res.redirect('/admin/account')
                    });
                })


            });
        });


    //
    //
    // });
});

router.post('/account/doctor', function (req, res, next) {
    if (!req.body.email || !req.body.password) {
        return res.status(400).send();
    }
    const name = req.body.name || "No Name";
    req.app.db.models.Doctor.create({
        name: name,
        degree: req.body.degree
    }, (err, doctor) => {
        if (err) {
            res.status(400).send();
        }
        req.app.db.models.User.encryptPassword(req.body.password, (err, hash) => {
            if (err) {
                return res.status(500).send();
            }
            req.app.db.models.User.create({
                email: req.body.email, password: hash, roles: {
                    patient: doctor._id
                }
            }, function (err, newUser) {
                if (err) {
                    console.log(err);
                    res.status(500).send();
                }

                doctor.user = newUser._id;
                doctor.save((err, updatedDoctor) => {
                    if (err) {
                        res.status(500).send();
                    }
                    //updatedDoctor.user = newUser;
                    //res.json(updatedDoctor);
                    res.redirect('/admin/account')
                });
            })


        });
    });


    //
    //
    // });
});

router.post('/account/patient', function (req, res, next) {
    if (!req.body.email || !req.body.password) {
        return res.status(400).send();
    }
    const name = req.body.name || "No Name";
    req.app.db.models.Patient.create({
        name: name,
        degree: req.body.degree
    }, (err, doctor) => {
        if (err) {
            res.status(400).send();
        }
        req.app.db.models.User.encryptPassword(req.body.password, (err, hash) => {
            if (err) {
                return res.status(500).send();
            }
            req.app.db.models.User.create({
                email: req.body.email, password: hash, roles: {
                    patient: doctor._id
                }
            }, function (err, newUser) {
                if (err) {
                    console.log(err);
                    res.status(500).send();
                }

                doctor.user = newUser._id;
                doctor.save((err, updatedDoctor) => {
                    if (err) {
                        res.status(500).send();
                    }
                    //updatedDoctor.user = newUser;
                    //res.json(updatedDoctor);
                    res.redirect('/admin/account')
                });
            })


        });
    });


    //
    //
    // });
});

router.get('/news', function (req, res, next) {
    req.app.db.models.Article.find().then(articles => res.render('admin/news_list', {articles: articles}));
});

router.get('/news/editor', function (req, res, next) {
    res.render('admin/news_editor');
});

router.get('/news/:articleId', function (req, res, next) {
    req.app.db.models.Article.findById(req.params.articleId).then(article => {
        res.render('admin/news_single', {article: article});
    }).catch(e => res.status(404).send(e.message));
});

router.post('/news/publish', multer.single('image'), function (req, res, next) {
    console.log(req.file);
    if (req.file) {
        cloudinary.v2.uploader.upload_stream({resource_type: 'image'},
            function (error, result) {
                if (!error) {
                    save_article(req, res, result.url);
                } else {
                    save_article(req, res, null);
                }
            })
            .end(req.file.buffer);
    } else {
        save_article(req, res);
    }
});

router.get('/logout', function (req, res, next) {
    console.log('logging out');
    req.logout();
    res.redirect('/admin/login');
});

function save_article(req, res, imgUrl) {

    req.app.db.models.Article.create({
        title: req.body.title,
        content: req.body.content,
        imgUrl: imgUrl
    }, function (err) {
        if (!err) {
            res.redirect('/admin/news');
        } else {
            res.status(500).send(err.message);
        }
    })
}

module.exports = router;
