var express = require('express');
var router = express.Router();
const passport = require('passport');

const moment = require('moment');


const ensureDoctor = function (req, res, next) {
    if (req.isAuthenticated() && req.user.roles.doctor) {
        console.log('Authenticated role: ', req.user.roles.admin);
        return next();
    }
    req.flash('error','Chưa đăng nhập');
    res.redirect("/doctor/login");
};

router.get('/login', function (req, res, next) {
    res.render('login', {loginUrl: '/doctor/login'});
});

router.post('/login', function (req, res, next) {
    if (!req.body.email || !req.body.password) {
        console.log("missing param");
        res.status(400).send();
    }
    passport.authenticate('local', function (err, user, info) {
        if (err) {
            console.log(err);
            return next(err);
        }
        console.log(user);
        if (!user || !user.roles.doctor) {
            req.flash('error','Sai tên tài khoản hoặc mật khẩu');
            return res.redirect('/doctor/login');
        }

        req.login(user, function (err) {
            if (err) {
                return next(err);
            }
            return res.redirect('/doctor');
        });
    })(req, res, next);
});

router.post('/profile', ensureDoctor, function (req, res, next) {
    req.app.db.models.Doctor.findByIdAndUpdate(req.user.roles.doctor, {
        name: req.body.name,
        degree: req.body.degree
    }, function (err) {
        if (err) {
            console.log(err);
        }
        res.redirect('/doctor');
    });
});

router.get('/test.json', function (req, res, next) {
    return res.json({msg: "Hello"});
});

router.get('/chat.json', function (req, res, next) {
    req.app.db.models.Patient.find({doctor: req.query.id}).populate('last_message').then(msgs => res.json(msgs))
        .catch(e => {
            console.log(e);
            res.status(400).send(e.message);
        })
});

router.get('/mailbox', ensureDoctor, function (req, res, next) {
    const page = req.query.page || 1;
    const doctor = req.app.db.models.Doctor.findById(req.user.roles.doctor);
    const patients = req.app.db.models.Patient.paginate({
        doctor: req.user.roles.doctor
    }, {
        populate: "last_message",
        page: page,
        limit: 10
    });
    Promise.all([doctor, patients]).then(values => {
        res.render("doctor/mailbox.ejs", {
            doctor: values[0],
            patients: values[1]
        });
    }).catch(e => {
        console.log(e);
        res.status(400).send(e.message);
    })
});

router.get('/mailbox/:patientId', ensureDoctor, function (req, res, next) {
    const patient = req.app.db.models.Patient.findById(req.params.patientId).populate('user', 'email');
    const doctor = req.app.db.models.Doctor.findById(req.user.roles.doctor);
    const messages = req.app.db.models.Message.find({
        doctor: req.user.roles.doctor,
        patient: req.params.patientId
    }).sort('timestamp');
    Promise.all([patient, doctor, messages]).then(values => {
        res.render('doctor/mail_single', {patient: values[0], doctor: values[1], messages: values[2]});
    }).catch(e => res.status(400).send(e.message));
});

router.post('/mailbox/:patientId', ensureDoctor, function (req, res, next) {
    req.app.db.models.Message.create({
        patient: req.params.patientId,
        doctor: req.user.roles.doctor,
        msg: req.body.msg,
        fromPatient: false
    }).then(message => {

        console.log('Sending notification');
        //Send notification
        req.app.db.models.Patient.findById(req.params.patientId)
            .populate('doctor').exec((err, patient) => {
            if (!err && patient && patient.push_token) {
                const payload = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
                    to: patient.push_token,
                    collapse_key: req.user.roles.doctor,
                    notification: {
                        title: patient.doctor.name,
                        body: message.msg
                    }
                };

                req.app.fcm.send(payload, function (err, response) {
                    if (err) {
                        console.log("Something has gone wrong!");
                    } else {
                        console.log("Successfully sent with response: ", response);
                    }
                });
            }
            else {
                console.log('Push token empty')
            }
        });
        res.redirect(req.params.patientId);
    });
});

router.get('/messages.json', function (req, res, next) {
    req.app.db.models.Message.find({doctor: req.query.doctor_id, patient: req.query.patient_id})
        .sort("timestamp")
        .then(msgs => {
            if (req.query.group && msgs.length > 0) {
                const groups = [];
                let lastDate = msgs[0].timestamp;
                let lastFromPatient = msgs[0].fromPatient;
                let dayBuffer = [];
                let minBuffer = [msgs[0]];
                for (let i = 1; i < msgs.length; i++) {
                    const curDate = msgs[i].timestamp;
                    if (lastDate.getDay() === curDate.getDay() &&
                        lastDate.getMonth() === curDate.getMonth() &&
                        lastDate.getFullYear() === curDate.getFullYear()) {
                        if (curDate.getTime() - lastDate.getTime() <= 60000 && msgs[i].fromPatient === lastFromPatient) {
                            minBuffer.push(msgs[i]);
                        } else {
                            dayBuffer.push(minBuffer);
                            minBuffer = [msgs[i]];
                        }
                    } else {
                        groups.push(dayBuffer);
                        dayBuffer = [];
                        minBuffer = [msgs[i]];
                    }
                    lastDate = curDate;
                }
                //Flush the buffer
                dayBuffer.push(minBuffer);
                groups.push(dayBuffer);
                res.json(groups);
            } else {
                res.json(msgs);
            }
        })
        .catch(e => {
            console.log(e);
            res.status(400).send(e.message);
        })

});

router.get('/', ensureDoctor, function (req, res, next) {
    const page = req.query.page || 1;
    const limit = req.query.limit || 10;
    const doctor = req.app.db.models.Doctor.findById(req.user.roles.doctor).then(doctor => {
        doctor.email = req.user.email;
        return doctor;
    });

    const patients = req.app.db.models.Patient.paginate({
        doctor: req.user.roles.doctor
    }, {
        populate: [{
            path: 'user',
            select: 'email'
        }, 'latest_visit'],
        page: page,
        limit: limit
    });

    Promise.all([doctor, patients]).then(values => {
        res.render('doctor/index', {doctor: values[0], patients: values[1]})
    }).catch(e => res.send(e.message));
    // patients.then(result => {
    //     console.log(result);
    //     res.send(result);
    // }).catch(err => {
    //     console.log(err);
    //     res.status(400).send(err.message);
    // })

});

router.get('/records', ensureDoctor, function (req, res, next) {
    const patient_id = req.query.patient_id;
    const patient = req.app.db.models.Patient.findById(patient_id);
    const records = req.app.db.models.MedicalHistory.find({patient: patient_id});
    const doctor = req.app.db.models.Doctor.findById(req.user.roles.doctor);
    Promise.all([patient, doctor, records]).then(values => {
        res.render('doctor/med_record.ejs', {patient: values[0], doctor: values[1], records: values[2]})
    }).catch(e => res.send(e.message));
});

router.get('/patient/:patientId/all', ensureDoctor, async (req, res) => {
    try {
        const patient_id = req.params.patientId;

        const patient = await req.app.db.models.Patient.findById(patient_id).populate('user','email');
        const records = await req.app.db.models.MedicalHistory.find({patient: patient_id});
        const doctor = await req.app.db.models.Doctor.findById(req.user.roles.doctor);


        const sitting = await req.app.db.models.SittingTime.aggregate([
            {$match: {chairId: patient.chairId}},
            {
                $group: {
                    _id: {month: {$month: "$timeStart"}, day: {$dayOfMonth: "$timeStart"}, year: {$year: "$timeStart"}},
                    totalTime: {
                        $sum: {$subtract: ["$timeEnd", "$timeStart"]}
                    },
                    sortKey: { "$first": "$timeStart" },
                    count: {$sum: 1}
                }
            },
            {$sort: {sortKey: 1}},
            {$limit: 10}
        ]);
        console.log(sitting);
        const sittingStats = {
            labels: sitting.map(x => moment([x._id.year, x._id.month - 1, x._id.day]).format('dddd, DD-MM-YYYY')),
            timePerDays: sitting.map(x => Math.round(x.totalTime /360000) / 10),
            countPerDays: sitting.map(x => x.count)
        };

        res.render('doctor/patient_detail.ejs', {
            patient: patient,
            doctor: doctor,
            records: records,
            sittingStats: sittingStats
        });
    } catch (e) {
        console.log('Mongo Error: ', e);
        res.status(500).send();
    }
});

router.get('/patient/:patientId/test_results', ensureDoctor, async (req, res) => {
    try {
        const patient_id = req.params.patientId;

        const patient = await req.app.db.models.Patient.findById(patient_id).populate('user','email');
        const records = await req.app.db.models.MedicalHistory.find({patient: patient_id}).sort({date:-1});
        const doctor = await req.app.db.models.Doctor.findById(req.user.roles.doctor);

        res.render('doctor/patient_test_result.ejs', {
            patient: patient,
            doctor: doctor,
            records: records
        });
    } catch (e) {
        console.log('Mongo Error: ', e);
        res.status(500).send();
    }
});

router.post('/patient/:patientId/test_results', ensureDoctor, async (req, res) => {
   const data = {
       name: req.body.name,
       doctorName: req.body.doctorName,
       date:moment(req.body.date +  " " + req.body.time,"MM-DD-YYYY HH:mm").toDate(),
       symptoms: req.body.symptoms,
       conclusion: req.body.conclusion,
       treatment: req.body.treatment,
       note: "",
       patient: req.params.patientId
   };
   try {
       await req.app.db.models.MedicalHistory.create(data);
       res.redirect('/doctor/patient/' + req.params.patientId + '/test_results');
   }catch (e) {
       console.log('Mongo Error: ', e);
       res.status(500).send();
   }
});
router.get('/patient/:patientId/test_results/new', function (req, res, next) {
    console.log('hi');
    res.render('doctor/med_record_add_or_edit');
});

router.get('/patient/:patientId/sitting', ensureDoctor, async (req, res) => {
    try {
        const patient_id = req.params.patientId;

        const patient = await req.app.db.models.Patient.findById(patient_id).populate('user','email');
        const sittingTimes = await req.app.db.models.SittingTime.find({chairId: patient.chairId}).sort({timeStart:-1});
        const doctor = await req.app.db.models.Doctor.findById(req.user.roles.doctor);

        res.render('doctor/patient_sitting.ejs', {
            patient: patient,
            doctor: doctor,
            sittingTimes: sittingTimes
        });
    } catch (e) {
        console.log('Mongo Error: ', e);
        res.status(500).send();
    }
});

router.get('/logout', function (req, res, next) {
    console.log('logging out');
    req.logout();
    res.redirect('/doctor/login');
});

module.exports = router;