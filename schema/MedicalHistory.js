'use strict';

exports = module.exports = function (app, mongoose) {
    const medicalSchema = new mongoose.Schema({
        name: String,
        doctorName: String,
        date: {type: Date, default: Date.now},
        symptoms: String,
        conclusion: String,
        treatment: String,
        note: String,
        patient: {type: mongoose.Schema.Types.ObjectId, ref: 'Patient'}
    });
    //adminGroupSchema.plugin(require('./plugins/pagedFind'));
    medicalSchema.index({date: 1}, {unique: true});
    medicalSchema.set('autoIndex', (app.get('env') === 'development'));
    app.db.model('MedicalHistory', medicalSchema);
};