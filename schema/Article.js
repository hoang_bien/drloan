'use strict';

exports = module.exports = function (app, mongoose) {
    const articleSchema = new mongoose.Schema({
        title: String,
        content: String,
        imgUrl: String,
        admin: {type: mongoose.Schema.Types.ObjectId, ref: 'Admin'},
        timestamp: {type: Date, default: Date.now}
    });
    articleSchema.methods.getContentPlainText = function () {
        const h2p = require('html2plaintext');
        return h2p(this.content);
    };
    //adminGroupSchema.plugin(require('./plugins/pagedFind'));
    articleSchema.index({timestamp: 1}, {unique: true});
    articleSchema.set('autoIndex', (app.get('env') === 'development'));
    app.db.model('Article', articleSchema);

};