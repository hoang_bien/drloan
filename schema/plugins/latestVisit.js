module.exports = exports = function latestVisitPlugin(schema, options) {
    schema.pre('save',function (next) {
        const patient = require('../Patient');

        patient.latest_visit = this._id;
        next();
    })
};