'use strict';

exports = module.exports = function (app, mongoose) {
    const messageSchema = new mongoose.Schema({
        patient: {type: mongoose.Schema.ObjectId, ref: 'Patient'},
        doctor: {type: mongoose.Schema.ObjectId, ref: 'Doctor'},
        isRead: {type: Boolean, default: false},
        timestamp: {type: Date, default: Date.now},
        fromPatient: {type: Boolean, default: false},
        msg: {type: String, required: true},
    });
    //adminGroupSchema.plugin(require('./plugins/pagedFind'));
    messageSchema.index({timestamp: 1}, {unique: true});
    messageSchema.set('autoIndex', (app.get('env') === 'development'));
    messageSchema.post('save',function (message, next) {
        app.db.models.Patient.findByIdAndUpdate(message.patient,{last_message: message._id},function (err) {
            if (err){
                console.log('Save last message failed: ', err);
                next(err);
            }else {
                next();
            }
        });
    });
    app.db.model('Message', messageSchema);


};