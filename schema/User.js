'use strict';

exports = module.exports = function(app, mongoose) {
    const userSchema = new mongoose.Schema({
        password: String,
        email: { type: String, unique: true },
        roles: {
            admin: { type: mongoose.Schema.Types.ObjectId, ref: 'Admin' },
            patient: { type: mongoose.Schema.Types.ObjectId, ref: 'Patient' },
            doctor: {type: mongoose.Schema.Types.ObjectId, ref: 'Doctor'}
        },
        isActive: String,
        timeCreated: { type: Date, default: Date.now },
        resetPasswordToken: String,
        resetPasswordExpires: Date,
        search: [String],
    });
    userSchema.methods.canPlayRoleOf = function(role) {
        if (role === "admin" && this.roles.admin) {
            return true;
        }

        if (role === "patient" && this.roles.patient) {
            return true;
        }

        if (role === "doctor" && this.roles.doctor) {
            return true;
        }

        return false;
    };
    userSchema.methods.defaultReturnUrl = function() {
        var returnUrl = '/';

        if (this.canPlayRoleOf('doctor')) {
            returnUrl = '/doctor/';
        }
        if (this.canPlayRoleOf('admin')) {
            returnUrl = '/admin/';
        }
        return returnUrl;
    };

    userSchema.statics.encryptPassword = function(password, done) {
        const bcrypt = require('bcrypt');
        bcrypt.genSalt(10, function(err, salt) {
            if (err) {
                return done(err);
            }

            bcrypt.hash(password, salt, function(err, hash) {
                done(err, hash);
            });
        });
    };

    userSchema.statics.validatePassword = function(password, hash, done) {
        const bcrypt = require('bcrypt');
        bcrypt.compare(password, hash, function(err, res) {
            done(err, res);
        });
    };

    //userSchema.plugin(require('./plugins/pagedFind'));
    userSchema.index({ email: 1 }, { unique: true });
    userSchema.index({ timeCreated: 1 });
    userSchema.index({ search: 1 });
    userSchema.set('autoIndex', (app.get('env') === 'development'));
    app.db.model('User', userSchema);
};