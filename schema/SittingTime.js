'use strict';

exports = module.exports = function (app, mongoose) {
    const sittingTimeSchema = new mongoose.Schema({
        timeStart: Date,
        timeEnd: Date,
        chairId: {type: String, required: true}
    });

    sittingTimeSchema.index({timeStart: 1}, {unique: true});
    //sittingTimeSchema.index({timeEnd: 1}, {unique: true});
    sittingTimeSchema.set('autoIndex', (app.get('env') === 'development'));
    app.db.model('SittingTime', sittingTimeSchema);
};