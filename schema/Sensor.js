'use strict';

exports = module.exports = function (app, mongoose) {
    const sensorSchema = new mongoose.Schema({
        data1: {type: Number, required: true},
        data2: {type: Number, required: true},
        data3: {type: Number, required: true},
        data4: {type: Number, required: true},
        data5: {type: Number, required: true},
        data6: {type: Number, required: true},
        timestamp: {type: Date, default: Date.now},
        chairId: {type: String, required: true}
    });

    sensorSchema.index({timestamp: 1}, {unique: true});
    sensorSchema.set('autoIndex', (app.get('env') === 'development'));
    app.db.model('Sensor', sensorSchema);
};