'use strict';

exports = module.exports = function (app, mongoose) {
    const PatientChair = new mongoose.Schema({
        chairId: {type: String, required: true},
        patientId: {type: mongoose.Schema.ObjectId, ref: 'Patient'},
        startDate: {type: Date, default: Date.now},
        endDate: Date, // null neu nhu ghe do van con dung
        inUsed: {type: Boolean, required: true}

    });

    PatientChair.index({patientId: 1}, {unique: true});
    PatientChair.index({chairId: 1}, {unique: true});
    PatientChair.set('autoIndex', (app.get('env') === 'development'));
    app.db.model('PatientChair', PatientChair);
};