'use strict';

exports = module.exports = function (app, mongoose) {
    const doctorSchema = new mongoose.Schema({
        user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        // isVerified: { type: String, default: '' },
        // verificationToken: { type: String, default: '' },
        name: {type: String, default: ''},
        // isMale: {type: Boolean, default: true},
        // phone: {type: String, default: ''},
        // address: {type: String, default: ''},
        // chairId: {type: String,required: true},
        degree: String,
        imgUrl: {type: String, default: ''},
        search: [String]
    });
    // accountSchema.plugin(require('./plugins/pagedFind'));
    doctorSchema.methods.getImgUrlOrDefault = function(){
        return this.imgUrl || "/assets/img/default-avatar.png";
    };

    doctorSchema.index({user: 1});
    doctorSchema.index({search: 1});
    doctorSchema.set('autoIndex', (app.get('env') === 'development'));
    app.db.model('Doctor', doctorSchema);
};