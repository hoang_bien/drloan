'use strict';

exports = module.exports = function (app, mongoose) {
    const patientSchema = new mongoose.Schema({
        user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        // isVerified: { type: String, default: '' },
        // verificationToken: { type: String, default: '' },
        name: {type: String, default: ''},
        isMale: {type: Boolean, default: true},
        phone: {type: String, default: ''},
        address: {
            street: {type: String, default: ''},
            district: {type: String, default: ''},
            city: {type: String, default: ''}
        },
        isInitProfile:{type: Boolean, default: false},
        currentChairId: {type: String, required: true},
        birthday: Date,
        imgUrl: {type: String, default: ''},
        doctor: {type: mongoose.Schema.ObjectId,ref:'Doctor'},
        search: [String],
        push_token: String,
        chairId: String,
        latest_visit:{type: mongoose.Schema.Types.ObjectId, ref: 'MedicalHistory'},
        last_message: {type: mongoose.Schema.Types.ObjectId, ref: 'Message'}
    });

    patientSchema.methods.getAddressString = function(){
        return [this.address.street, this.address.district, this.address.city].join(', ');
    };

    patientSchema.methods.getImgUrlOrDefault = function(){
        return this.imgUrl || "/assets/img/default-avatar.png";
    };

    patientSchema.plugin(require('mongoose-paginate'));
    patientSchema.index({user: 1});
    patientSchema.index({search: 1});
    patientSchema.set('autoIndex', (app.get('env') === 'development'));


    app.db.model('Patient', patientSchema);
};