'use strict';
exports = module.exports = function (io) {
    io.on('connection', (socket) => {

        socket.on('username', (userName) => {

            this.users.push({
                id : socket.id,
                userName : userName
            });

            // let len = this.users.length;
            // len--;

            // this.io.emit('userList',this.users,this.users[len].id);
        });

        socket.on('getMsg', (data) => {
            socket.broadcast.to(data.toid).emit('sendMsg',{
                msg:data.msg,
                name:data.name
            });
        });

        socket.on('disconnect',()=>{

            for(let i=0; i < this.users.length; i++){

                if(this.users[i].id === socket.id){
                    this.users.splice(i,1);
                }
            }
            //this.io.emit('exit',this.users);
        });

    });
};


