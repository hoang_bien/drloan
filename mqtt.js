'use strict';
const sensors = {}
exports = module.exports = function (app, client) {

    let connected = {};

    client.on('connect', () => {
        //client.subscribe('status/+');
        client.subscribe('chair/sensor/+');
    });

    client.on('message', (topic, message) => {
        if (topic.indexOf('chair/sensor') !== -1) {
            return handleDataFromChair(topic.split('/')[2], message);
        }
        if (topic.indexOf('chair/connected') !== -1) {
            return handleChairConnected(topic.split('/')[2], message);
        } else {
            console.log('No handler for topic %s', topic);
        }
    });

    function handleChairConnected(chairId, message) {
        console.log('Gateway connection status %s', message);
        connected = (message.toString() === 'true')
    }

    async function handleDataFromChair(chairId, message) {
        //TODO: Store data into the database
        const data = JSON.parse(message);
        //
        let data1 = data.data1;
        let data2 = data.data2;
        let data3 = data.data3;
        let data4 = data.data4;
        let data5 = data.data5;
        let data6 = data.data6;
        // config deltal
        let delta = 50;
        let delta2 = 100;
        let deltatime = 30 * 1000;
        // init status
        let last = sensors[chairId.toString()] || false;
        let current = (data1 > delta || data2 > delta || data3 > delta2 || data4 > delta2 || data5 > delta2 || data6 > delta2);
        // cache status
        sensors[chairId.toString()] = current;
        console.log('last', last, 'current', current);
        if (last === current) {
            // data not turning so do nothing!
            console.log('data not turning so do nothing!');
            return;
        }
        try {
            let last_record = await app.db.models.SittingTime.find({
                chairId: chairId
            }).sort({timeEnd: -1}).limit(1);
            if (current) {
                if (last_record[0]) {
                    let timeEnd = last_record[0].timeEnd;
                    let current = new Date();
                    console.log(timeEnd);
                    console.log('diff time from last record',current- timeEnd);
                    if (current - timeEnd < deltatime) {
                        // don't create new record
                        console.log('use last record as a new record!');
                        return;
                    }
                }

                // create new record
                let create_record = await app.db.models.SittingTime.create({
                    chairId: chairId,
                    timeStart: new Date()
                });
                console.log('create_record', create_record);
                return;
            }
            let update_record = await app.db.models.SittingTime.findOneAndUpdate({
                _id: last_record[0]._id
            }, {
                $set: {
                    timeEnd: new Date()
                }
            });
            console.log('update_record', update_record);
        } catch (e) {
            console.error(e);
        }
    }
};