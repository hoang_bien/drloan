'use strict';

exports.mongodb = 'mongodb://admin:yahoo24@ds111993.mlab.com:11993/drloan';
exports.projectName = 'DrLoan';
exports.systemEmail = 'hbien34@gmail.com';
exports.cryptoKey = 'k3yb0ardc4t';
exports.jwt_secret= process.env.JWT_SECRET || 'naruto';
exports.fcm_secret = 'AAAAUFoUc1I:APA91bHKivCGqblBJPyMyoYFNuGIwlI6mbQHIO_Lpfm4jN078MymFmlfspKeNzo1JAnS_kgD0iI_jDm0owFliDKqIlSaR5gtbUAdNkNcJTtfdOGwDQWdsMBhKQaOKIdhNKYOrxEl2ndl';
exports.loginAttempts = {
    forIp: 50,
    forIpAndUser: 7,
    logExpiration: '20m'
};
exports.cloudary = {
    cloud_name: 'drloan',
    api_key: '127835669473479',
    api_secret: 'i6DnsR74L1NoG6yKDeapnEw0ycY'
};

exports.mqtt = {
    uri: process.env.MQTT_URI || 'm15.cloudmqtt.com',
    port: process.env.MQTT_PORT || 14517,
    username: process.env.MQTT_USERNAME || 'tavncmmi',
    password: process.env.MQTT_PASSWORD || 'ryl7wVmJgvzh'
};

exports.requireAccountVerification = false;
exports.smtp = {
    from: {
        name: process.env.SMTP_FROM_NAME || exports.projectName + ' Website',
        address: process.env.SMTP_FROM_ADDRESS || 'your@email.addy'
    },
    credentials: {
        user: process.env.SMTP_USERNAME || 'your@email.addy',
        password: process.env.SMTP_PASSWORD || 'bl4rg!',
        host: process.env.SMTP_HOST || 'smtp.gmail.com',
        ssl: true
    }
};
exports.oauth = {
    twitter: {
        key: process.env.TWITTER_OAUTH_KEY || '',
        secret: process.env.TWITTER_OAUTH_SECRET || ''
    },
    facebook: {
        key: process.env.FACEBOOK_OAUTH_KEY || '',
        secret: process.env.FACEBOOK_OAUTH_SECRET || ''
    },
    github: {
        key: process.env.GITHUB_OAUTH_KEY || '',
        secret: process.env.GITHUB_OAUTH_SECRET || ''
    },
    google: {
        key: process.env.GOOGLE_OAUTH_KEY || '',
        secret: process.env.GOOGLE_OAUTH_SECRET || ''
    },
    tumblr: {
        key: process.env.TUMBLR_OAUTH_KEY || '',
        secret: process.env.TUMBLR_OAUTH_SECRET || ''
    }
};