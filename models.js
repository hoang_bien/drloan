'use strict';

exports = module.exports = function(app, mongoose) {
    //then regular docs
    require('./schema/User')(app, mongoose);
    require('./schema/AdminGroup')(app,mongoose);
    require('./schema/Admin')(app,mongoose);
    require('./schema/Patient')(app,mongoose);
    require('./schema/Doctor')(app,mongoose);
    require('./schema/Message')(app,mongoose);
    require('./schema/Sensor')(app,mongoose);
    require('./schema/Article')(app,mongoose);
    require('./schema/SittingTime')(app,mongoose);
    require('./schema/MedicalHistory')(app,mongoose);
};