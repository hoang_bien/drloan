var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const session = require('express-session');

var indexRouter = require('./routes/index');
var adminRouter = require('./routes/admin');
var apiRouter = require('./routes/api');
var authRouter = require('./routes/auth');
var doctorRouter = require('./routes/doctor');

var mongoose = require('mongoose');
const mqtt = require('mqtt');
var config = require('./config');
const passport = require('passport');
const FCM = require('fcm-node');
const moment = require('moment');
const cloudinary = require('cloudinary');
const fcm = new FCM(config.fcm_secret);
var flash = require('express-flash');
var app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
moment.locale('vi');
app.config = config;
app.fcm = fcm;
mongoose.connect(config.mongodb, {useNewUrlParser: true});
// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;
//Get the default connection
app.db = mongoose.connection;
//Bind connection to error event (to get notification of connection errors)
app.db.on('error', console.error.bind(console, 'MongoDB connection error:'));
//config data models
require('./models')(app, mongoose);
require('./passport')(app, passport);

cloudinary.config(config.cloudary);

const client = mqtt.connect({
    host: config.mqtt.uri,
    port: config.mqtt.port,
    username: config.mqtt.username,
    password: config.mqtt.password
});
require('./mqtt')(app, client);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(
    session({
        secret: config.jwt_secret,
        resave: false,
        saveUninitialized: false
    })
);

passport.serializeUser(function (user, cb) {
    console.log("serializeUser", user);
    cb(null, user._id);
});

passport.deserializeUser(function (id, cb) {
    console.log("deserializeUser", id);
    app.db.models.User.findById(id, function (err, user) {
        if (err) {
            return cb(err);
        }
        cb(null, user);
    });
});

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

app.use(flash());

// Custom flash middleware -- from Ethan Brown's book, 'Web Development with Node & Express'
// app.use(function(req, res, next){
//     // if there's a flash message in the session request, make it available in the response, then delete it
//     res.locals.sessionFlash = req.session.sessionFlash;
//     delete req.session.sessionFlash;
//     next();
// });

app.use(function(req, res, next){
    res.locals.moment = moment;
    next();
});

app.use('/', indexRouter);
app.use('/admin', adminRouter);
app.use('/auth', authRouter);
app.use('/doctor',doctorRouter);
app.use('/api', passport.authenticate('jwt', {session: false}), apiRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});
module.exports = app;
